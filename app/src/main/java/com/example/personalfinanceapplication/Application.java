package com.example.personalfinanceapplication;

import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;

public class Application extends android.app.Application {
    private String token;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
    }


    public String getAppToken() {
        return token;
    }

    public void setAppToken(String token) {
        this.token = token;
    }
}

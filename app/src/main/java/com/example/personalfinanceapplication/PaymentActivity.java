package com.example.personalfinanceapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class PaymentActivity extends AppCompatActivity {

    private static final String TAG = "PaymentActivity";

    private ArrayList<Transaction> transactions = new ArrayList<>();

    EditText amountInput, noteInput;
    String token;
    SwipeRefreshLayout swiper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        noteInput = (EditText)findViewById(R.id.note_input);
        amountInput = (EditText)findViewById(R.id.amount_input);
        swiper = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTransactions();
            }
        });

        SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
                Context.MODE_PRIVATE);
        String key = sharedPref.getString(getString(R.string.token_key), "");
        token = "Bearer " + key;

        Log.d(TAG, "onCreate: "+ token);

        getTransactions();
    }

    public void createTransaction(View view){
        noteInput.clearFocus();
        amountInput.clearFocus();

        String note = noteInput.getText().toString();
        String amount = amountInput.getText().toString();



        AndroidNetworking.post("http://128.199.144.102/api/transactions")
                .addHeaders("Authorization", token)
                .addBodyParameter("note", note)
                .addBodyParameter("amount", amount)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Toast.makeText(getApplicationContext(), "Successfully Added!", Toast.LENGTH_SHORT).show();
                        noteInput.setText("");
                        amountInput.setText("");
                        noteInput.clearFocus();
                        amountInput.clearFocus();

                        getTransactions();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void getTransactions(){
        Log.d(TAG, "getTransactions: called");
        AndroidNetworking.get("http://128.199.144.102/api/transactions")
                .addHeaders("Authorization", token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {

                            JSONArray res = response.getJSONArray("success");
                            Log.d(TAG, "onResponse: "+ res);
                            if (res != null) {
                                transactions = new ArrayList<>();
                                for (int i=0;i<res.length();i++){
                                    JSONObject item = res.getJSONObject(i);
                                    Transaction transaction = new Transaction(item.getString("note"), item.getString("amount"), item.getString("created_at"));
                                    transactions.add(transaction);
                                }
                            }
                            Log.d(TAG, "transactions: "+ Arrays.toString(transactions.toArray()));
                            swiper.setRefreshing(false);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(getApplicationContext(), "Got transactions!", Toast.LENGTH_SHORT).show();
                        initRecyclerView();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: called");

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(transactions, this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

}

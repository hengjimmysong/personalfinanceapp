package com.example.personalfinanceapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
                Context.MODE_PRIVATE);
        String key = sharedPref.getString(getString(R.string.token_key), "");
        Log.d("KEY_AUTH_ACT", key);

        if(key != ""){
            Log.d("HAS_KEY_AUTH_ACT", key);
            AuthActivity.this.authCheck(key);
        } else{
            Log.d("NO_KEY_AUTH_ACT", key);
            AuthActivity.this.goToSighIn();
        }
    }

    public void authCheck(final String key){
        String token = "Bearer " + key;
        Log.d("CHECK_AUTH_ACT", token);
        AndroidNetworking.get("http://128.199.144.102/api/user")
                .addHeaders("Authorization", token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Log.d("SUCCESS_AUTH_ACT", key);
                        try {
                            SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
                                    Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.user_key), response.getString("name"));
                            editor.commit();
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "abc", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                        Intent myIntent = new Intent(AuthActivity.this, MainActivity.class);
                        AuthActivity.this.startActivity(myIntent);
                        AuthActivity.this.finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.d("AUTH_ERROR", "onError: " +error.getErrorDetail());
                        // handle error
                        AuthActivity.this.goToSighIn();
                    }
                });
    }

    public void goToSighIn() {
        Intent myIntent = new Intent(AuthActivity.this, SignInActivity.class);
        AuthActivity.this.startActivity(myIntent);

        AuthActivity.this.finish();
    }
}

package com.example.personalfinanceapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = "MainActivity";

  CardView dailySpending, weeklySpending, monthlySpending;
  String token;
  TextView dailyAmount, weeklyAmount, monthlyAmount;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
            Context.MODE_PRIVATE);
    String name = sharedPref.getString(getString(R.string.user_key), "");

    Toast.makeText(getApplicationContext(), "Welcome, " + name, Toast.LENGTH_SHORT).show();

    String key = sharedPref.getString(getString(R.string.token_key), "");
    token = "Bearer " + key;

    dailyAmount = (TextView)findViewById(R.id.daily_amount);
    weeklyAmount = (TextView)findViewById(R.id.weekly_amount);
    monthlyAmount = (TextView)findViewById(R.id.monthly_amount);
  }

  @Override
  protected void onStart() {
    super.onStart();

    getDailySpending();
    getWeeklySpending();
    getMonthlySpending();
  }

  public void getDailySpending(){
    AndroidNetworking.get("http://128.199.144.102/api/transactions/today")
            .addHeaders("Authorization", token)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @Override
              public void onResponse(JSONObject response) {
                // do anything with response

                try {
                  Log.d(TAG, "onResponse: "+ String.format("%.02f", Float.parseFloat(response.getString("success"))));
                  dailyAmount.setText("$ " + String.format("%.02f", Float.parseFloat(response.getString("success"))));
                } catch (JSONException e) {
                  e.printStackTrace();
                }

              }
              @Override
              public void onError(ANError error) {
                // handle error
                Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT).show();
              }
            });
  }

  public void getWeeklySpending(){
    AndroidNetworking.get("http://128.199.144.102/api/transactions/weekly")
            .addHeaders("Authorization", token)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @Override
              public void onResponse(JSONObject response) {
                // do anything with response

                try {
                  Log.d(TAG, "onResponse: weekly"+ String.format("%.02f", Float.parseFloat(response.getString("success"))));
                  weeklyAmount.setText("$ " + String.format("%.02f", Float.parseFloat(response.getString("success"))));
                } catch (JSONException e) {
                  e.printStackTrace();
                }

              }
              @Override
              public void onError(ANError error) {
                // handle error
                Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT).show();
              }
            });
  }

  public void getMonthlySpending(){
    AndroidNetworking.get("http://128.199.144.102/api/transactions/monthly")
            .addHeaders("Authorization", token)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @Override
              public void onResponse(JSONObject response) {
                // do anything with response

                try {
                  Log.d(TAG, "onResponse: monthly"+ String.format("%.02f", Float.parseFloat(response.getString("success"))));
                  monthlyAmount.setText("$ " + String.format("%.02f", Float.parseFloat(response.getString("success"))));
                } catch (JSONException e) {
                  e.printStackTrace();
                }

              }
              @Override
              public void onError(ANError error) {
                // handle error
                Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT).show();
              }
            });
  }


  public void logOut(View view){

    SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
            Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(getString(R.string.token_key), "");
    editor.commit();

    Intent myIntent = new Intent(MainActivity.this, SignInActivity.class);
    MainActivity.this.startActivity(myIntent);

    MainActivity.this.finish();
  }

  public void goToTransactions(View view){
    Intent myIntent = new Intent(MainActivity.this, PaymentActivity.class);
    MainActivity.this.startActivity(myIntent);
  }
}

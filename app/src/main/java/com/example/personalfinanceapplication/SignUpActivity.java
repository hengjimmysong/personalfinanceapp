package com.example.personalfinanceapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends Activity {

    EditText emailInp, nameInp, passwordInp, passwordCInp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        nameInp = (EditText)findViewById(R.id.name_inp);
        emailInp = (EditText)findViewById(R.id.email_inp);
        passwordInp = (EditText)findViewById(R.id.password_inp);
        passwordCInp = (EditText)findViewById(R.id.password_c_inp);
    }

    public void onSignUp(View view) {
        Toast.makeText(getApplicationContext(), "Signing Up...", Toast.LENGTH_LONG).show();
        String name = nameInp.getText().toString();
        String email = emailInp.getText().toString();
        String password = passwordInp.getText().toString();
        String passwordC = passwordCInp.getText().toString();

        AndroidNetworking.post("http://128.199.144.102/api/register")
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .addBodyParameter("c_password", passwordC)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();
                        try {
                            SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
                                    Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.token_key), response.getJSONObject("success").getString("token"));
                            editor.putString(getString(R.string.user_key), response.getJSONObject("success").getString("name"));
                            editor.commit();
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "There was an error", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                        Intent myIntent = new Intent(SignUpActivity.this, MainActivity.class);
                        SignUpActivity.this.startActivity(myIntent);
                        SignUpActivity.this.finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(), "Incorrect Credentials", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void goToSignIn(View view) {
        Intent myIntent = new Intent(SignUpActivity.this, SignInActivity.class);
        SignUpActivity.this.startActivity(myIntent);

        SignUpActivity.this.finish();
    }
}

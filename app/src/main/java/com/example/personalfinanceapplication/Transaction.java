package com.example.personalfinanceapplication;

import java.util.Date;

public class Transaction {
    public String note;
    public String amount;
    public String created_at;

    public Transaction( String note, String amount, String created_at) {
        this.amount = amount;
        this.note = note;
        this.created_at = created_at;
    }
}

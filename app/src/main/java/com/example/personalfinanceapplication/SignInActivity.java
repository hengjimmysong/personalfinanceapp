package com.example.personalfinanceapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class SignInActivity extends Activity {
    private static final String TAG = "SignInActivity";

    EditText emailInput, passwordInput;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sign_in);

      emailInput = (EditText)findViewById(R.id.emailInp);
      passwordInput = (EditText)findViewById(R.id.passwordInp);
  }

  public void onSignIn(View view) {
    Toast.makeText(getApplicationContext(), "Signing In...", Toast.LENGTH_LONG).show();
      String email = emailInput.getText().toString();
      String password = passwordInput.getText().toString();

      Log.d("INPUT", "onSignIn: "+ email + " -- " + password);
    AndroidNetworking.post("http://128.199.144.102/api/login")
            .addBodyParameter("email", email)
            .addBodyParameter("password", password)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @Override
              public void onResponse(JSONObject response) {
                // do anything with response
                  try {
                      Log.d(TAG, "onResponse: " + "runned1");

                      SharedPreferences sharedPref = getSharedPreferences(String.valueOf(R.string.shared_pref_key),
                              Context.MODE_PRIVATE);
                      SharedPreferences.Editor editor = sharedPref.edit();
                      Log.d(TAG, "onResponse: " + response.getJSONObject("success").getString("token"));
                      Log.d(TAG, "onResponse: " + response.getJSONObject("success").getString("name"));
                      editor.putString(getString(R.string.token_key), response.getJSONObject("success").getString("token"));
                      editor.putString(getString(R.string.user_key), response.getJSONObject("success").getString("name"));
                      editor.commit();
                      Log.d(TAG, "onResponse: " + "runned2");
                  } catch (JSONException e) {
                      Log.d(TAG, "onResponse: " + "catched");

                      Toast.makeText(getApplicationContext(), "There was an error", Toast.LENGTH_SHORT).show();
                      e.printStackTrace();
                  }

                  Intent myIntent = new Intent(SignInActivity.this, MainActivity.class);
                  SignInActivity.this.startActivity(myIntent);
                  SignInActivity.this.finish();
              }
              @Override
              public void onError(ANError error) {
                // handle error
                  Log.d(TAG, "onError: "+  error.getMessage());
                Toast.makeText(getApplicationContext(), "Incorrect Credentials", Toast.LENGTH_SHORT).show();
              }
            });
  }

    public void goToSignUp(View view) {
        Intent myIntent = new Intent(SignInActivity.this, SignUpActivity.class);
        SignInActivity.this.startActivity(myIntent);

        SignInActivity.this.finish();
    }
}

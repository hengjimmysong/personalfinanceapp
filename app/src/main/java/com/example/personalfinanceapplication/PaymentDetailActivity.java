package com.example.personalfinanceapplication;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.NumberFormat;

public class PaymentDetailActivity extends AppCompatActivity {
    private static final String TAG = "PaymentDetailActivity";
    TextView note,date,time,amount, amountKHR;

    NumberFormat myFormat = NumberFormat.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail);

        note = (TextView)findViewById(R.id.detail_note);
        date = (TextView)findViewById(R.id.detail_date);
        time = (TextView)findViewById(R.id.detail_time);
        amount = (TextView)findViewById(R.id.detail_amount);
        amountKHR = (TextView)findViewById(R.id.detail_amount_khr);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            note.setText( extras.getString("note"));
            date.setText("Date: "+extras.getString("date"));
            time.setText("Time: "+extras.getString("time"));
            amount.setText( "$ "+ extras.getString("amount"));
            Log.d(TAG, "onCreate: "+ "KHR " + String.format("%.00f", Float.parseFloat(extras.getString("amount"))*4000));
            amountKHR.setText("KHR " + String.format("%.00f", Float.parseFloat(extras.getString("amount"))*4000));
            //The key argument here must match that used in the other activity
        }
    }
}

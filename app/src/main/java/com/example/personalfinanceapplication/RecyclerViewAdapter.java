package com.example.personalfinanceapplication;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "PAYMENT_RECYCLER_VIEW";

    private ArrayList<Transaction> transactionArrayList = new ArrayList<>();
    private Context mcontext;
    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
    SimpleDateFormat myFormat2 = new SimpleDateFormat("EEE, d MMM - h:mm a");
    SimpleDateFormat myFormat3 = new SimpleDateFormat("EEE, d MMM");
    SimpleDateFormat myFormat4 = new SimpleDateFormat("h:mm a");
    final long HOUR = 3600*1000;

    public RecyclerViewAdapter(ArrayList<Transaction> transactionArrayList, Context mcontext) {
        this.transactionArrayList = transactionArrayList;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called");

        holder.position = position;

        holder.note.setText(transactionArrayList.get(position).note);

        holder.amount.setText("$ " + String.format("%.02f", Float.parseFloat(transactionArrayList.get(position).amount)));


        try {

            holder.date.setText(myFormat2.format(new Date(myFormat.parse(transactionArrayList.get(position).created_at.toString()).getTime() + 7 * HOUR)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return transactionArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView note, date, amount;
        int position;
        RelativeLayout paymentItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            note = itemView.findViewById(R.id.payment_item_note);
            date = itemView.findViewById(R.id.payment_item_date);
            amount = itemView.findViewById(R.id.payment_item_amount);
            paymentItem = itemView.findViewById(R.id.payment_item);
            paymentItem.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(mcontext, PaymentDetailActivity.class);
                    try {
                        myIntent.putExtra("date", myFormat3.format(new Date(myFormat.parse(transactionArrayList.get(position).created_at.toString()).getTime() + 7 * HOUR)));
                        myIntent.putExtra("time", myFormat4.format(new Date(myFormat.parse(transactionArrayList.get(position).created_at.toString()).getTime() + 7 * HOUR)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    myIntent.putExtra("note", transactionArrayList.get(position).note);
                    myIntent.putExtra("amount", String.format("%.02f", Float.parseFloat(transactionArrayList.get(position).amount)));
                    mcontext.startActivity(myIntent);
                }
            });
        }
    }
}
